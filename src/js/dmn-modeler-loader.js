(function ($, Drupal) {
  'use strict';
  var DmnViewer = require('dmn-js/lib/Modeler');

  $.fn.dmnModeler = function (options) {
    this.each(function () {
      var element = this;
      var src = element.getAttribute('data-src');

      element.style.minHeight = options.height;
      element.style.position = 'relative';

      $.get(src, function (xml, status, data) {
        var viewer = new DmnViewer({
          container: element
        });

        viewer.importXML(data.responseText, function (err) {
          if (err) { throw err; }

          $('.dmn-diagram', element).css({position: 'absolute'});
        });
      });
    });
  };

  Drupal.behaviors.bpmnIoDmnModeler = {
    attach: function (context, settings) {
      $('[dmn-modeler]', context).dmnModeler(settings.bpmnIo.dmnModeler);
    }
  };
})(jQuery, Drupal);
