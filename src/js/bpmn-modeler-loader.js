(function ($, Drupal) {
  'use strict';
  var BpmnViewer = require('bpmn-js/lib/Modeler');

  function viewerReady(viewer, options) {
    // amazing API... (just kidding)
    viewer.get('canvas').zoom('fit-viewport', 'auto');
    debugger;
  }


  $.fn.bpmnModeler = function (options) {
    this.each(function () {
      var element = this;
      var src = element.getAttribute('data-src');

      element.style.minHeight = options.height;
      element.style.position = 'relative';

      $.get(src, function (xml, status, data) {
        var viewer = new BpmnViewer({
          container: element
        });

        viewer.importXML(data.responseText, function (err) {
          if (err) { throw err; }

          $('.bjs-container', element).css({position: 'absolute'});
          viewerReady(viewer, options);
        });
      });
    });
  };

  Drupal.behaviors.bpmnIoBpmnModeler = {
    attach: function (context, settings) {
      $('[bpmn-modeler]', context).bpmnModeler(settings.bpmnIo.bpmnModeler);
    }
  };
})(jQuery, Drupal);
