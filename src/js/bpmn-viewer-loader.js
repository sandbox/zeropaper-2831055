(function ($, Drupal) {
  'use strict';
  var BpmnViewer = require('bpmn-js');

  $.fn.bpmnViewer = function (options) {
    this.each(function () {
      var element = this;
      var src = element.getAttribute('data-src');

      element.style.minHeight = options.height;
      element.style.position = 'relative';

      $.get(src, function (xml, status, data) {
        var viewer = new BpmnViewer({
          container: element
        });

        viewer.importXML(data.responseText, function (err) {
          if (err) { throw err; }

          $('.bjs-container', element).css({position: 'absolute'});
          // amazing API... (just kidding)
          viewer.get('canvas').zoom('fit-viewport', 'auto');
        });
      });
    });
  };

  Drupal.behaviors.bpmnIoBpmnViewer = {
    attach: function (context, settings) {
      $('[bpmn-viewer]', context).bpmnViewer(settings.bpmnIo.bpmnViewer);
    }
  };
})(jQuery, Drupal);
