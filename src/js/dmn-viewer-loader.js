(function ($, Drupal) {
  'use strict';
  var DmnViewer = require('dmn-js');

  $.fn.dmnViewer = function (options) {
    this.each(function () {
      var element = this;
      var src = element.getAttribute('data-src');

      element.style.minHeight = options.height;
      element.style.position = 'relative';

      $.get(src, function (xml, status, data) {
        var viewer = new DmnViewer({
          container: element
        });

        viewer.importXML(data.responseText, function (err) {
          if (err) { throw err; }

          $('.dmn-diagram', element).css({position: 'absolute'});
        });
      });
    });
  };

  Drupal.behaviors.bpmnIoDmnViewer = {
    attach: function (context, settings) {
      $('[dmn-viewer]', context).dmnViewer(settings.bpmnIo.dmnViewer);
    }
  };
})(jQuery, Drupal);
