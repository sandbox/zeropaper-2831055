(function ($, Drupal) {
  'use strict';
  var CmmnViewer = require('cmmn-js/lib/NavigatedViewer');

  $.fn.cmmnNavigatedViewer = function (options) {
    this.each(function () {
      var element = this;
      var src = element.getAttribute('data-src');

      element.style.minHeight = options.height;
      element.style.position = 'relative';

      $.get(src, function (xml, status, data) {
        var viewer = new CmmnViewer({
          container: element
        });

        viewer.importXML(data.responseText, function (err) {
          if (err) { throw err; }

          $('.cjs-container', element).css({position: 'absolute'});
          // amazing API... (just kidding)
          viewer.get('canvas').zoom('fit-viewport', 'auto');
        });
      });
    });
  };

  Drupal.behaviors.bpmnIoCmmnNavigatedViewer = {
    attach: function (context, settings) {
      $('[cmmn-navigated-viewer]', context).cmmnNavigatedViewer(settings.bpmnIo.cmmnNavigatedViewer);
    }
  };
})(jQuery, Drupal);
