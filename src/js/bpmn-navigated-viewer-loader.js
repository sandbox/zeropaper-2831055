(function ($, Drupal) {
  'use strict';
  var BpmnViewer = require('bpmn-js/lib/NavigatedViewer');

  $.fn.bpmnNavigatedViewer = function (options) {
    this.each(function () {
      var element = this;
      var src = element.getAttribute('data-src');

      element.style.minHeight = options.height;
      element.style.position = 'relative';

      $.get(src, function (xml, status, data) {
        var viewer = new BpmnViewer({
          container: element
        });

        viewer.importXML(data.responseText, function (err) {
          if (err) { throw err; }

          $('.bjs-container', element).css({position: 'absolute'});
          // amazing API... (just kidding)
          viewer.get('canvas').zoom('fit-viewport', 'auto');
        });
      });
    });
  };

  Drupal.behaviors.bpmnIoBpmnNavigatedViewer = {
    attach: function (context, settings) {
      $('[bpmn-navigated-viewer]', context).bpmnNavigatedViewer(settings.bpmnIo.bpmnNavigatedViewer);
    }
  };
})(jQuery, Drupal);
