<?php

namespace Drupal\bpmn_io\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;

/**
 * Formatter that shows a diagram based on a CMMN file.
 *
 * @FieldFormatter(
 *   id = "bpmn_io_cmmn",
 *   module = "bpmn_io",
 *   label = @Translation("CMMN"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class CMMNFileFormatter extends BPMNFileFormatter {
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $settings = $this->getSettings();

    if (!$items->isEmpty()) {
      foreach ($items as $delta => $item) {
        $file = file_load($item->target_id);
        $url = file_create_url($file->getFileUri());
        $elements[$delta] = [];
        $elements[$delta]['#markup'] = '<div cmmn-' . $settings['bpmn_io_integration_type'] . ' data-src="' . $url . '"></div>';
      }

      $entryName = bmpn_io_camelcase('cmmn-' . $settings['bpmn_io_integration_type']);
      $elements['#attached']['drupalSettings']['bpmnIo'][$entryName] = [
        'height' => $settings['bpmn_io_viewer_height']
      ];
      $elements['#attached']['library'][] = 'bpmn_io/cmmn-' . $settings['bpmn_io_integration_type'];
    }

    return $elements;
  }

}
