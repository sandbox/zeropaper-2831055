<?php

namespace Drupal\bpmn_io\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\SafeMarkup;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Formatter that shows a diagram based on a BPMN file.
 *
 * @FieldFormatter(
 *   id = "bpmn_io_bpmn",
 *   module = "bpmn_io",
 *   label = @Translation("BPMN"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class BPMNFileFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
      'bpmn_io_integration_type' => 'viewer',
      'bpmn_io_viewer_height' => '50vh'
    ) + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $settings = $this->getSettings();

    $form['bpmn_io_viewer_height'] = [
      '#title' => t('Viewer height'),
      '#type' => 'textfield',
      '#default_value' => $settings['bpmn_io_viewer_height'],
      '#required' => TRUE
    ];

    $form['bpmn_io_integration_type'] = [
      '#title' => t('Integration type'),
      '#type' => 'select',
      '#options' => [
        'viewer' => t('Viewer'),
        'navigated-viewer' => t('Viewer with navigation'),
        'modeler' => t('Modeler'),
      ],
      '#default_value' => $settings['bpmn_io_integration_type'],
      '#required' => TRUE
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $settings = $this->getSettings();

    switch ($settings['bpmn_io_integration_type']) {
      case 'viewer':
        $summary[] = t('Viewer');
        break;

      case 'navigated-viewer':
        $summary[] = t('Viewer with navigation');
        break;

      case 'modeler':
        $summary[] = t('Modeler');
        break;
    }

    $summary[] = t('Height') . ' ' . $settings['bpmn_io_viewer_height'];

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $settings = $this->getSettings();

    if (!$items->isEmpty()) {
      foreach ($items as $delta => $item) {
        $file = file_load($item->target_id);
        $url = file_create_url($file->getFileUri());
        $elements[$delta] = [];
        $elements[$delta]['#markup'] = '<div bpmn-' . $settings['bpmn_io_integration_type'] . ' data-src="' . $url . '"></div>';
      }

      $entryName = bmpn_io_camelcase('bpmn-' . $settings['bpmn_io_integration_type']);
      $elements['#attached']['drupalSettings']['bpmnIo'][$entryName] = [
        'height' => $settings['bpmn_io_viewer_height']
      ];
      $elements['#attached']['library'][] = 'bpmn_io/bpmn-' . $settings['bpmn_io_integration_type'];
    }

    return $elements;
  }

}
