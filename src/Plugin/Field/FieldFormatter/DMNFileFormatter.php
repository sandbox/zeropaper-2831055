<?php

namespace Drupal\bpmn_io\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\SafeMarkup;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Formatter that shows a diagram based on a DMN file.
 *
 * @FieldFormatter(
 *   id = "bpmn_io_dmn",
 *   module = "bpmn_io",
 *   label = @Translation("DMN"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class DMNFileFormatter extends BPMNFileFormatter {
  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $settings = $this->getSettings();

    $form['bpmn_io_integration_type']['#options'] = [
      'viewer' => t('Viewer'),
      'modeler' => t('Modeler'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $settings = $this->getSettings();

    if (!$items->isEmpty()) {
      foreach ($items as $delta => $item) {
        $file = file_load($item->target_id);
        $url = file_create_url($file->getFileUri());
        $elements[$delta] = [];
        $elements[$delta]['#markup'] = '<div dmn-' . $settings['bpmn_io_integration_type'] . ' data-src="' . $url . '"></div>';
      }

      $entryName = bmpn_io_camelcase('dmn-' . $settings['bpmn_io_integration_type']);
      $elements['#attached']['drupalSettings']['bpmnIo'][$entryName] = [
        'height' => $settings['bpmn_io_viewer_height']
      ];
      $elements['#attached']['library'][] = 'bpmn_io/dmn-' . $settings['bpmn_io_integration_type'];
    }

    return $elements;
  }

}
