# BPMN, CMMN and DMN Integration

This module provides file formatters for the following file types:

* **BPMN:** Business Protocol Model and Notation
  * Viewer
  * Viewer with navigation
  * Modeler
* **CMMN:** Case Management Model And Notation
  * Viewer
  * Modeler
* **DMN:** Decision Model and Notation
  * Viewer

## Installation

You will need [node.js](https://nodejs.org) (and npm) prior to install.

```sh
npm i
```

## Author

Valentin `zeropaper` Vago